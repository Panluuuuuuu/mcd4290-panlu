//to check the code, uncomment the corresponding question.

question1();
question2();
question3();
question4();
question5();

function question1(){
    let output = "" //empty output, fill this so that it can print onto the page.
    
    //Question 1 here 
    let positiveOdd = []
    let negativeEven = []
    let myArray = [54, -16, 80, 55, -74, 73, 26, 5, -34, -73, 19, 63, -55, -61, -14, -19, -51, -17, -25]
    for (let i = 0; i<myArray.length; i++){
        if(myArray[i]>0) {
            if(myArray[i]%2 !== 0)
                positiveOdd.push(myArray[i])
        }
        else{
            if(myArray[i]%2 === 0)
                negativeEven.push(myArray[i])
        }
    }
    
    output += "Positive Odd: " + positiveOdd + " Negative Even: " + negativeEven
    
    let outPutArea = document.getElementById("outputArea1") //this line will find the element on the page called "outputArea1"
    outPutArea.innerText = output //this line will fill the above element with your output.
}

function question2(){
    let output = "" 
    //Question 2 here
    let face1 = 0
    let face2 = 0
    let face3 = 0
    let face4 = 0
    let face5 = 0
    let face6 = 0
    for (let i =0; i<60000; i ++) {
        let diceFace = Math.floor((Math.random() * 6 ) +1 )
        if (diceFace === 1) {
            face1++;
        }
        else if(diceFace === 2) {
            face2++;
        }
        else if(diceFace === 3) {
            face3++;
        }
        else if(diceFace === 4) {
            face4++;
        }
        else if(diceFace === 5) {
            face5++;
        }
        else {
            face6++;
        }
        
    }
    output += "Frequency of die rolls " + "\n 1: " + face1 + "\n 2: " + face2 + "\n 3: " + face3 + "\n 4: " +face4 + "\n 5: " + face5 + "\n 6: " + face6
    
    let outPutArea = document.getElementById("outputArea2") 
    outPutArea.innerText = output 
}


function question3(){
    let output = "" 
    
    //Question 3 here
    let frequencies = [0, 0, 0, 0, 0, 0, 0]
    for (let i=0; i<60000; i++ ) {
        let diceFace = Math.floor((Math.random()*6) + 1)
        frequencies[diceFace] ++
    }
    output += "Frequency of die rolls " + "\n 1: " + frequencies[1] + "\n 2: " + frequencies[2] + "\n 3: " + frequencies[3] + "\n 4: " + frequencies[4] + "\n 5: " + frequencies[5] + "\n 6: " + frequencies[6]
    
    let outPutArea = document.getElementById("outputArea3")
    outPutArea.innerText = output 
}


function question4(){
    let output = "" 
    
    //Question 4 here
    var diceRolls = {
        Frequencies: {
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        },
        Total: 60000,
        Exceptions: ""
    }
    let rand;
    for (let i=0; i<= diceRolls.Total; i++ ) {
        rand = Math.floor((Math.random()*6) + 1)
        diceRolls.Frequencies[rand] ++
    }
    
    for(prop in diceRolls.Frequencies) {
        output += prop + ": " + diceRolls.Frequencies[prop] + "<br>"
        if(diceRolls.Frequencies[prop] > 10100 || diceRolls.Frequencies[prop] < 9900) {
            diceRolls.Exceptions += prop + ", "
        }
    }
    output += "Exceptions: " + diceRolls.Exceptions
    
    let outPutArea = document.getElementById("outputArea4") 
    outPutArea.innerHTML = output 
}

function question5(){
    let output = "" 
    let incomeTax = ""
    //Question 5 here 
    let person = {
        name: "Jane",
        income: 127050
    }
    if (person.income>=0) {
        if(person.income<=18200) {
            incomeTax = 0
        }
        else if(18201<=person.income && person.income<=37000) {
            incomeTax = (person.income - 18200)*0.19
        }
        else if(37001<=person.income && person.income<=90000) {
            incomeTax = 3572 + (person.income - 37000)*0.325
        }
        else if(90001<=person.income && person.income<=180000) {
            incomeTax = 20797 + (person.income - 90000)*0.37
        }
        else {
            incomeTax = 54097 + (person.income - 180000)*0.45
        }
    }
    output += person.name + "'s income is: $" + person.income + ", and her tax owed is: $" + incomeTax
 
    
    let outPutArea = document.getElementById("outputArea5") 
    outPutArea.innerText = output 
}