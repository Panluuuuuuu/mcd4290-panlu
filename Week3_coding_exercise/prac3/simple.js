
// Question 1
let output1 = ""

function objectToHTML(object) {
    let formatStr = '';
    for (let prop in object) {
         formatStr += prop + ":" + object[prop] + ",\n"
    }
    return formatStr
}

let testObj = {
    number: 1,
    string: "abc",
    array: [5, 4, 3, 2, 1],
    boolean: true
}

output1 = objectToHTML(testObj)

let outPutArea1 = document.getElementById("outputArea1")
outPutArea1.innerText = output1;






//Question 2
let output2 = ""

let mult = function(num1, num2) {
    return num1 * num2
}

function add(num1, num2) {
    return num1 + num2
}

function flexible(fOperation, operand1, operand2) {
    let result = fOperation(operand1, operand2);
    return result;
}

output2 += flexible(add, 3, 5) + '<br/>'+ "\n";  //named function
output2 += flexible(mult, 3, 5) + '<br/';        //anonymous function

let outPutArea2 = document.getElementById("outputArea2")
outPutArea2.innerHTML = output2;






// Question 3
/*

Find the minnimum value in an array
1. create two array, one for minimum value and one for maximum value
2. compare the previosu number and current number
3. if the current number less than previous number, save it in the minimum array at index 0
4. if the current number greater than the previous number, save the previous number in the minimum array at index 0
5. keep looping and find the minimum value, if there are a new minimum number, replace it to the number saved before at index 0 in the minimum array.
6. when the loop finished, return the index 0 in the minimum number array

Find the minnimum value in an array
The same concept of the find miminum number method but change some compare sequence.

*/






// Question 4
let output4 = "";

var values = [4, 3, 6, 12, 1, 3, 8];
let minNum = [];
let maxNum = [];
let currentNum;

function extrmeValues(myArray) {
  for(let i = 0; i < myArray.length; i++){
    if(i>0) {
      currentNum = myArray[i-1]
      if(currentNum > myArray[i]) {
        minNum[0] = (myArray[i])
      }
      else {
        if (currentNum < minNum[0])
        minNum[0] = currentNum
      }
    }
  }
  return minNum[0]
}

function maxValues(myArray) {
  for(let i = 0; i < myArray.length; i++){
    if(i>0) {
      currentNum = myArray[i-1]
      if(currentNum > myArray[i]) {
        maxNum[0] = (currentNum)
      }
      else {
        if (currentNum > maxNum[0])
        maxNum[0] = currentNum
      }
    }
  }
  return maxNum[0]
}

output4 += extrmeValues(values) + "\n"
output4 += maxValues(values)

let outPutArea4 = document.getElementById("outputArea4")
outPutArea4.innerText = output4;