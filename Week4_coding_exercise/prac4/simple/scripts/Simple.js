/*
• A user should be able to enter a number in each text field.
• When the “Do It!” button is pressed, a function should be called that adds the two numbers
together and outputs the result after the blue equals sign.
• The result should be coloured blue if the number is positive, or red if it is negative.
*/

//button onclick to call the function
//create a function
//get number1 and number2 from input1 and input2
//adds the two numbers and save the answer
//outputs the results after the blue equals sign

function doIt() {
    //get the reference
    let num1 = Number(document.getElementById("number1").value);
    let num2 = Number(document.getElementById("number2").value);
    let answerRef = document.getElementById("answer")
    let answer = num1 + num2;
    answerRef.innerHTML = answer;
    let evenOddRef = document.getElementById("evenOdd")

    if(answer>0) {
        answerRef.className = "positive"
    }
    else {
        answerRef.className = "negative"
    }
    if (answer %2 === 0){
        evenOddRef.className = "Even"
        evenOddRef.innerHTML = "(Even)"
        }
    else {
        evenOddRef.className = "Odd"
        evenOddRef.innerHTML = "(odd)"
    } 
}

